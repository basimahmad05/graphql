package com.basim.graphqltask.repository

import com.apollographql.apollo.api.Response
import com.basim.graphqltask.CharactersListQuery

interface CharacterRepository {

    suspend fun queryCharactersList(): Response<CharactersListQuery.Data>

}