package com.basim.graphqltask.repository

import com.apollographql.apollo.api.Response
import com.apollographql.apollo.coroutines.await
import com.basim.graphqltask.CharactersListQuery
import com.basim.graphqltask.networking.RickAndMortyApi
import javax.inject.Inject

class CharacterRepositoryImpl @Inject constructor(
    private val webService: RickAndMortyApi
) : CharacterRepository {

    override suspend fun queryCharactersList(): Response<CharactersListQuery.Data> {
        return webService.getApolloClient().query(CharactersListQuery()).await()
    }

}